import { nextTick } from 'vue';
import * as svg from '@element-plus/icons-vue';
import SvgIcon from '@/components/svgIcon/index.vue';

/**
 * 导出全局注册 element plus svg 图标
 * @param app vue 实例
 * @description 使用：https://element-plus.gitee.io/zh-CN/component/icon.html
 */
export function elSvg(app) {
    const icons = svg;
    for (const i in icons) {
        app.component(`ele-${icons[i].name}`, icons[i]);
    }
    app.component('SvgIcon', SvgIcon);
}

/**
 * 图片懒加载
 * @param el dom 目标元素
 * @param arr 列表数据
 * @description data-xxx 属性用于存储页面或应用程序的私有自定义数据
 */
export const lazyImg = (el, arr) => {
    const io = new IntersectionObserver(res => {
        res.forEach(v => {
            if (v.isIntersecting) {
                const { img, key } = v.target.dataset;
                v.target.src = img;
                v.target.onload = () => {
                    io.unobserve(v.target);
                    arr[key]['loading'] = false;
                };
            }
        });
    });
    nextTick(() => {
        document.querySelectorAll(el).forEach(img => io.observe(img));
    });
};

/**
 * 对象深克隆
 * @param obj 源对象
 * @returns 克隆后的对象
 */
export function deepClone(obj) {
    let newObj;
    try {
        newObj = obj.push ? [] : {};
    } catch (error) {
        newObj = {};
    }
    for (let attr in obj) {
        if (obj[attr] && typeof obj[attr] === 'object') {
            newObj[attr] = deepClone(obj[attr]);
        } else {
            newObj[attr] = obj[attr];
        }
    }
    return newObj;
}

/**
 * 判断是否是移动端
 */
export function isMobile() {
    if (
        navigator.userAgent.match(
            /('phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone')/i
        )
    ) {
        return true;
    } else {
        return false;
    }
}

/**
 * 判断数组对象中所有属性是否为空，为空则删除当前行对象
 * @description @感谢大黄
 * @param list 数组对象
 * @returns 删除空值后的数组对象
 */
export function handleEmpty(list) {
    const arr = [];
    for (const i in list) {
        const d = [];
        for (const j in list[i]) {
            d.push(list[i][j]);
        }
        const leng = d.filter(item => item === '').length;
        if (leng !== d.length) {
            arr.push(list[i]);
        }
    }
    return arr;
}

/*
 * 转换行数据到表单
 */
export function changeRowToForm(row, formdata) {
    for (let attr in formdata) {
        if (formdata[attr] && isArray(formdata[attr])) {
            if (row[attr] && typeof row[attr] === 'string') {
                row[attr] = JSON.parse(row[attr]);
            } else {
                row[attr] = [];
            }
        }
    }
    return row;
}

/**
 * 统一批量导出
 * @method elSvg 导出全局注册 element plus svg 图标
 * @method useTitle 设置浏览器标题国际化
 * @method setTagsViewNameI18n 设置 自定义 tagsView 名称、 自定义 tagsView 名称国际化
 * @method lazyImg 图片懒加载
 * @method globalComponentSize() element plus 全局组件大小
 * @method deepClone 对象深克隆
 * @method isMobile 判断是否是移动端
 * @method handleEmpty 判断数组对象中所有属性是否为空，为空则删除当前行对象
 */
const other = {
    elSvg: app => {
        elSvg(app);
    },
    lazyImg: (el, arr) => {
        lazyImg(el, arr);
    },
    globalComponentSize: () => {
        return globalComponentSize();
    },
    deepClone: obj => {
        return deepClone(obj);
    },
    isMobile: () => {
        return isMobile();
    },
    handleEmpty: list => {
        return handleEmpty(list);
    }
};

// 统一批量导出
export default other;
