import request from '@/utils/request';
import { parseStrEmpty } from '@/utils/ruoyi';

export function postData(url, params) {
    return request({
        url: url,
        method: 'post',
        data: params
    });
}

export function getData(url, params) {
    return request({
        url: url,
        method: 'get',
        params
    });
}

/**
 * 获取上传Token
 * @param {String} parent_id
 * @param {String} type
 * @returns
 */
export function getUploadToken(parent_id, type = 'web') {
    return request({
        url: '/sys/storage/token',
        method: 'post',
        data: {
            parent_id,
            type
        }
    });
}

// 查询数据列表
export function listData(url, query) {
    return request({
        url: url + '/list',
        method: 'get',
        params: query
    });
}

// 查询数据列表
export function listAllData(url, query) {
    return request({
        url: url + '/all',
        method: 'get',
        params: query
    });
}

// 查询数据详细
export function getOneData(url, id) {
    return request({
        url: url + '/get',
        method: 'get',
        params: {
            id
        }
    });
}

// 新增数据
export function addData(url, data) {
    return postData(url + '/add', data);
}

// 修改数据
export function updateData(url, data) {
    return postData(url + '/update', data);
}

// 删除数据
export function delData(url, id) {
    return request({
        url: url + '/del',
        method: 'post',
        data: {
            id
        }
    });
}

// 删除数据
export function clearData(url) {
    return request({
        url: url + '/clear',
        method: 'post'
    });
}

// 删除数据
export function copyData(url, id) {
    return request({
        url: url + '/copy',
        method: 'post',
        data: {
            id
        }
    });
}

// 导出字典类型
export function exportData(url, query) {
    return request({
        url: url + '/export',
        method: 'get',
        params: query
    });
}

// 更新状态
export function changeStatus(url, data) {
    return request({
        url: url + '/status',
        method: 'post',
        data
    });
}
